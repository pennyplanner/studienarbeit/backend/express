// routes.js
const express = require('express');
const router = express.Router();
const logger = require('../logging/logger')
const db = require('../database/db');
const { generateUUID } = require('../functions/generateUUID');
const categoryController = require('../functions/categoryController')
const expenseController = require('../functions/expenseController')

const jwt = require('jsonwebtoken');
const jwksClient = require('jwks-rsa')

// ----- AUTHORIZATION -----

// function for JWT authorization
function authenticateJWT(req, res, next) {
  const { headers } = req;
  //logger.info({ message: 'Received headers', headers }); too many headers

  const authHeader = headers['authorization'];

  if (!authHeader || !authHeader.startsWith('Bearer ')) {
    logger.error('Not authorized: Missing or invalid authorization header');
    return res.status(401).json({ error: 'Not authorized' });
  }

  const token = authHeader.split(' ')[1];

  const client = jwksClient({
    jwksUri: `http://${process.env.KEYCLOAK_HOST}/auth/realms/${process.env.KEYCLOAK_REALM}/protocol/openid-connect/certs`
  });

  function getKey(header, callback) {
    client.getSigningKey(header.kid, function (err, key) {
      const signingKey = key.publicKey || key.rsaPublicKey;
      callback(null, signingKey);
    });
  }

  jwt.verify(token, getKey, (err, decoded) => {
    if (err) {
      if (err.name === 'TokenExpiredError') {
        logger.error('JWT expired', { expiredOn: err.expiredAt });
        return res.status(401).json({ error: 'JWT expired', expiredOn: err.expiredAt });
      } else {
        logger.error('Unauthorized');
        return res.status(401).json({ error: 'Unauthorized' });
      }
    }

    req.user = decoded;
    const { sub: userId, preferred_username: username } = decoded;

    logger.info(`User authenticated: ${userId} - ${username}`);
    req.userId = userId;
    req.username = username;
    next();
  });
}

//function to check for JWT authorization, but auth is not required
// an sich unnötig, weil vom rapiddoc kein authHeader geschickt wird. Vollständigkeitshalber trotzdem implementiert.
// -> UserID immer "Anonymous", Username immer "Unknown"
function authenticateJWT_optional(req, res, next) {
  const { headers } = req;
  //logger.info({ message: 'Received headers', headers }); too many headers

  const authHeader = headers['authorization'];

  if (!authHeader || !authHeader.startsWith('Bearer ')) {
    req.userId = null;
    req.username = null;
    return next();
  }

  const token = authHeader.split(' ')[1];

  const client = jwksClient({
    jwksUri: `http://${process.env.KEYCLOAK_HOST}/auth/realms/${process.env.KEYCLOAK_REALM}/protocol/openid-connect/certs`
  });

  function getKey(header, callback) {
    client.getSigningKey(header.kid, function (err, key) {
      const signingKey = key.publicKey || key.rsaPublicKey;
      callback(null, signingKey);
    });
  }

  jwt.verify(token, getKey, (err, decoded) => {
    if (err) {
      if (err.name === 'TokenExpiredError') {
        logger.error('JWT expired', { expiredOn: err.expiredAt });
        return res.status(401).json({ error: 'JWT expired', expiredOn: err.expiredAt });
      } else {
        logger.error('Unauthorized');
        return res.status(401).json({ error: 'Unauthorized' });
      }
    }

    req.user = decoded;
    const { sub: userId, preferred_username: username } = decoded;

    logger.info(`User authenticated: ${userId} - ${username}`);
    req.userId = userId;
    req.username = username;
    next();
  });
}

// ----- RESPONSES -----

function errorResponse(code_var, message_var, moreInfo, trace) {
  return {
    errors: [
      {
        code: code_var,
        message: message_var,
        more_info: moreInfo,
      },
    ],
    trace: trace || generateUUID(),
  };
}


// Defined routes based on the OpenAPI specification

// ----- STATUS & HEALTH -----

// GET /status
router.get('/status', authenticateJWT_optional, async (req, res) => {
  const trace = generateUUID();
  try {
    const timestamp = new Date().toISOString();

    const logData = {
      level: 'info',
      message: 'Read operation',
      operation: 'GET',
      '@timestamp': timestamp,
      'ecs.version': '1.11.0',
      event: {
        category: 'user_interaction',
        type: 'read',
      },
      user: {
        id: req.userId || 'Anonymous',
        username: req.username || 'Unknown',
      },
      statusCode: 200,
      trace: trace,
    };

    logger.info(logData);

    res.status(200).json({
      authors: ['Marc Griese', 'Miko Heuken'],
      api_version: '1.0.0',
    });
  } catch (error) {
    logger.error('Internal server error:', { error, statusCode: 500 });
    res.status(500).json({ error: 'Internal server error' });
  }
});

// GET /health
router.get('/health', authenticateJWT_optional, async (req, res) => {
  const trace = generateUUID()
  try {
    const timestamp = new Date().toISOString();

    //logging
    const logData = {
      level: 'info',
      message: 'Read operation',
      operation: 'GET',
      '@timestamp': timestamp,
      'ecs.version': '1.11.0',
      event: {
        category: 'user_interaction',
        type: 'read',
      },
      user: {
        id: req.userId || 'Anonymous',
        username: req.username || 'Unknown',
      },
      trace: trace,
    };

    const live = true;
    const ready = true;
    let connected = false;

    // Connect to the database and perform a ping query
    const { rows } = await db.query('SELECT 1');

    // If the query is successful, set "connected" to "true"
    if (rows.length > 0) {
      connected = true;
    }

    const databases = {
      assets: {
        connected: connected,
      },
    };

    if (live && ready && databases.assets.connected) {
      logger.info({ ...logData, statusCode: 200 });
      res.status(200).json({
        live,
        ready,
        databases,
      });
    } else {
      const errorCode = 'bad_request';
      const errorMessage = 'The service cannot be considered as live.';
      const moreInfo = 'It should be replaced or restarted,';

      const errorRes = errorResponse(errorCode, errorMessage, moreInfo, trace)

      logger.error('Service cannot be considered live:', { ...errorRes, statusCode: 503 });

      res.status(503).json(errorResponse);
    }
  } catch (error) {
    logger.error('Internal Server Error:', { error, statusCode: 500 });
    res.status(500).json({ error: 'Internal Server Error' });
  }
});

// GET /health/live
router.get('/health/live', authenticateJWT_optional, async (req, res) => {
  const trace = generateUUID();
  try {
    const timestamp = new Date().toISOString();

    const logData = {
      level: 'info',
      message: 'Read operation',
      operation: 'GET',
      '@timestamp': timestamp,
      'ecs.version': '1.11.0',
      event: {
        category: 'user_interaction',
        type: 'read',
      },
      user: {
        id: req.userId || 'Anonymous',
        username: req.username || 'Unknown',
      },
      statusCode: 200,
      trace: trace,
    };

    logger.info(logData);

    const liveness = true;

    if (liveness) {
      res.status(200).json({ live: true });
    } else {
      const errorCode = 'bad_request';
      const errorMessage = 'The service cannot be considered as live.';
      const moreInfo = 'It should be replaced or restarted.';

      const errorRes = errorResponse(errorCode, errorMessage, moreInfo, trace);

      logger.error('Service cannot be considered live:', { ...errorRes, statusCode: 503 });

      res.status(503).json(errorRes);
    }
  } catch (error) {
    logger.error('Internal Server Error:', { error, statusCode: 500 });
    res.status(500).json({ error: 'Internal Server Error' });
  }
});

// GET /health/ready
router.get('/health/ready', authenticateJWT_optional, async (req, res) => {
  const trace = generateUUID();
  try {
    const timestamp = new Date().toISOString();

    const logData = {
      level: 'info',
      message: 'Read operation',
      operation: 'GET',
      '@timestamp': timestamp,
      'ecs.version': '1.11.0',
      event: {
        category: 'user_interaction',
        type: 'read',
      },
      user: {
        id: req.userId || 'Anonymous',
        username: req.username || 'Unknown',
      },
      statusCode: 200,
      trace: trace,
    };

    logger.info(logData);

    const readiness = true;

    if (readiness) {
      res.status(200).json({ ready: true });
    } else {
      const errorCode = 'bad_request';
      const errorMessage = 'The service cannot be considered as ready.';
      const moreInfo = 'It should be replaced or restarted.';

      const errorRes = errorResponse(errorCode, errorMessage, moreInfo, trace);

      logger.error('Service cannot be considered ready:', { ...errorRes, statusCode: 503 });

      res.status(503).json(errorRes);
    }
  } catch (error) {
    logger.error('Internal Server Error:', { error, statusCode: 500 });
    res.status(500).json({ error: 'Internal Server Error' });
  }
});

// ----- CATEGORIES -----

// GET /categories
router.get('/categories', authenticateJWT_optional, async (req, res) => {
  const trace = generateUUID();
  try {
    const includeDeleted = req.query.include_deleted || false; // Default value: false

    let categories;
    if (includeDeleted === "true") {
      categories = await categoryController.getAllCategories()
    } else {
      categories = await categoryController.getActiveCategories();
    }

    if (!categories || categories.length === 0) {
      const logDataNoCategories = {
        level: 'info',
        message: 'No categories found.',
        operation: 'GET',
        '@timestamp': new Date().toISOString(),
        'ecs.version': '1.11.0',
        event: {
          category: 'user_interaction',
          type: 'read',
        },
        user: {
          id: req.userId || 'Anonymous',
          username: req.username || 'Unknown',
        },
        statusCode: 404,
        trace: trace,
      };

      logger.info(logDataNoCategories);

      return res.status(404).json({ error: 'No categories found' });
    }

    const categoryIds = categories.map(category => category.id).join(',');

    const logDataCategories = {
      level: 'info',
      message: 'Read operation',
      operation: 'GET',
      '@timestamp': new Date().toISOString(),
      'ecs.version': '1.11.0',
      event: {
        category: 'user_interaction',
        type: 'read',
      },
      user: {
        id: req.userId || 'Anonymous',
        username: req.username || 'Unknown',
      },
      object: {
        type: 'Category',
        id: categoryIds,
      },
      statusCode: 200,
      trace: trace,
    };

    logger.info(logDataCategories);

    res.status(200).json({ categories });
  } catch (error) {
    const logDataError = {
      level: 'error',
      message: 'Error occurred',
      operation: 'GET',
      '@timestamp': new Date().toISOString(),
      'ecs.version': '1.11.0',
      event: {
        category: 'user_interaction',
        type: 'read',
      },
      user: {
        id: req.userId || 'Anonymous',
        username: req.username || 'Unknown',
      },
      statusCode: 500,
      error,
      trace: trace,
    };

    logger.error(logDataError);

    res.status(500).json({ error: 'Internal server error' });
  }
});

// POST /categories
router.post('/categories', authenticateJWT, async (req, res) => {
  const trace = generateUUID();
  try {
    const { name } = req.body;

    // Validate input
    if (!name) {
      const errorCode = 'bad_request';
      const errorMessage = 'Name is required';
      const moreInfo = 'Please provide a name for the category';

      const errorRes = errorResponse(errorCode, errorMessage, moreInfo, trace);

      const logDataInvalidInput = {
        level: 'info',
        message: 'Invalid input for creating category',
        operation: 'POST',
        '@timestamp': new Date().toISOString(),
        'ecs.version': '1.11.0',
        event: {
          category: 'user_interaction',
          type: 'create',
        },
        user: {
          id: req.userId,
          username: req.username,
        },
        statusCode: 400,
        trace: trace,
      };

      logger.info(logDataInvalidInput);

      res.status(400).json(errorRes);
      return;
    }

    const newCategory = await categoryController.createCategory(name);

    const logDataCreatedCategory = {
      level: 'info',
      message: `Category created: ${name}`,
      operation: 'POST',
      '@timestamp': new Date().toISOString(),
      'ecs.version': '1.11.0',
      event: {
        category: 'user_interaction',
        type: 'create',
      },
      user: {
        id: req.userId,
        username: req.username,
      },
      object: {
        type: 'Category',
        id: newCategory.id,
      },
      statusCode: 201,
      trace: trace,
    };

    logger.info(logDataCreatedCategory);

    res.status(201).json(newCategory);
  } catch (error) {
    const logDataError = {
      level: 'error',
      message: 'Error occurred',
      operation: 'POST',
      '@timestamp': new Date().toISOString(),
      'ecs.version': '1.11.0',
      event: {
        category: 'user_interaction',
        type: 'create',
      },
      user: {
        id: req.userId,
        username: req.username,
      },
      statusCode: 500,
      error,
      trace: trace,
    };

    logger.error(logDataError);

    res.status(500).json({ error: 'Internal server error' });
  }
});

// GET /categories/:id
router.get('/categories/:id', authenticateJWT_optional, async (req, res) => {
  const trace = generateUUID();
  try {
    const categoryId = req.params.id;

    // Validate categoryId format before querying the database
    const isValid = await categoryController.isValidCategoryId(categoryId);

    if (!isValid) {
      const errorMessage = `Category with ID: ${categoryId} does not exist.`;

      const errorRes = errorResponse('not_found', errorMessage, 'Category not found');

      const logDataCategoryNotFound = {
        level: 'info',
        message: `Category with ID: ${categoryId} not found`,
        operation: 'GET',
        '@timestamp': new Date().toISOString(),
        'ecs.version': '1.11.0',
        event: {
          category: 'user_interaction',
          type: 'read',
        },
        user: {
          id: req.userId || 'Anonymous',
          username: req.username || 'Unknown',
        },
        statusCode: 404,
        trace: trace,
      };

      logger.info(logDataCategoryNotFound);

      res.status(404).json(errorRes);
      return;
    }

    const existingCategory = await categoryController.getCategoryById(categoryId);

    if (!existingCategory) {
      // Category wasn't found
      const errorMessage = `Category with ID: ${categoryId} does not exist.`;

      const errorRes = errorResponse('not_found', errorMessage, 'Category not found');

      const logDataCategoryNotFound = {
        level: 'info',
        message: `Category with ID: ${categoryId} not found`,
        operation: 'GET',
        '@timestamp': new Date().toISOString(),
        'ecs.version': '1.11.0',
        event: {
          category: 'user_interaction',
          type: 'read',
        },
        user: {
          id: req.userId || 'Anonymous',
          username: req.username || 'Unknown',
        },
        statusCode: 404,
        trace: trace,
      };

      logger.info(logDataCategoryNotFound);

      res.status(404).json(errorRes);
    } else {
      const timestamp = new Date().toISOString();

      // Logging
      const logDataCategoryFound = {
        level: 'info',
        message: `Read operation (GET) for Category ID: ${categoryId}`,
        operation: 'GET',
        '@timestamp': timestamp,
        'ecs.version': '1.11.0',
        event: {
          category: 'user_interaction',
          type: 'read',
        },
        user: {
          id: req.userId || 'Anonymous',
          username: req.username || 'Unknown',
        },
        object: {
          type: 'Category',
          id: categoryId,
        },
        statusCode: 200,
        trace: trace,
      };

      logger.info(logDataCategoryFound);

      res.status(200).json(existingCategory);
    }
  } catch (error) {
    logger.error('Error occurred: ', error);
    res.status(500).json({ error: 'Internal Server Error' });
  }
});

// PUT /categories/:id
router.put('/categories/:id', authenticateJWT, async (req, res) => {
  const trace = generateUUID();
  try {
    const categoryId = req.params.id;
    const { name } = req.body;

    // Validate input
    if (!name) {
      const errorCode = 'bad_request';
      const errorMessage = 'Name is required';
      const moreInfo = 'Please provide a name for the category';

      const errorRes = errorResponse(errorCode, errorMessage, moreInfo, trace);

      const logDataInvalidInput = {
        level: 'info',
        message: 'Invalid input for updating category',
        operation: 'PUT',
        '@timestamp': new Date().toISOString(),
        'ecs.version': '1.11.0',
        event: {
          category: 'user_interaction',
          type: 'update',
        },
        user: {
          id: req.userId,
          username: req.username,
        },
        statusCode: 400,
        trace: trace,
      };

      logger.info(logDataInvalidInput);

      res.status(400).json(errorRes);
      return;
    }

    // Check if category exists
    const existingCategory = await categoryController.getCategoryById(categoryId);

    if (!existingCategory) {
      // Create new category if ID not found
      const newCategory = await categoryController.createCategory(name, categoryId);

      const logDataCreateCategory = {
        level: 'info',
        message: `Category created: ${name}`,
        operation: 'PUT',
        '@timestamp': new Date().toISOString(),
        'ecs.version': '1.11.0',
        event: {
          category: 'user_interaction',
          type: 'create',
        },
        user: {
          id: req.userId,
          username: req.username,
        },
        object: {
          type: 'Category',
          id: newCategory.id,
        },
        statusCode: 201,
        trace: trace,
      };

      logger.info(logDataCreateCategory);

      res.status(201).json(newCategory); // Return newly created category
    } else {
      // Update existing category
      const updatedCategory = await categoryController.updateCategory(categoryId, name);

      const logDataUpdateCategory = {
        level: 'info',
        message: `Category updated: ${name}`,
        operation: 'PUT',
        '@timestamp': new Date().toISOString(),
        'ecs.version': '1.11.0',
        event: {
          category: 'user_interaction',
          type: 'update',
        },
        user: {
          id: req.userId,
          username: req.username,
        },
        object: {
          type: 'Category',
          id: updatedCategory.id,
        },
        statusCode: 200,
        trace: trace,
      };

      logger.info(logDataUpdateCategory);

      res.status(200).json(updatedCategory); // Return updated category
    }
  } catch (error) {
    const logDataError = {
      level: 'error',
      message: 'Error occurred',
      operation: 'PUT',
      '@timestamp': new Date().toISOString(),
      'ecs.version': '1.11.0',
      event: {
        category: 'user_interaction',
        type: 'update',
      },
      user: {
        id: req.userId,
        username: req.username,
      },
      statusCode: 500,
      error,
      trace: trace,
    };

    logger.error(logDataError);

    res.status(500).json({ error: 'Internal server error' });
  }
});

// DELETE /categories/{id}
router.delete('/categories/:id', authenticateJWT, async (req, res) => {
  const trace = generateUUID();
  try {
    const categoryId = req.params.id;

    if (!categoryId) {
      const errorCode = 'bad_request';
      const errorMessage = 'Category ID not provided';
      const moreInfo = 'Please provide a valid category ID for deletion';

      const errorRes = errorResponse(errorCode, errorMessage, moreInfo, trace);

      const logDataNoCategoryId = {
        level: 'info',
        message: 'No category ID provided for deletion',
        operation: 'DELETE',
        '@timestamp': new Date().toISOString(),
        'ecs.version': '1.11.0',
        event: {
          category: 'user_interaction',
          type: 'delete',
        },
        user: {
          id: req.userId,
          username: req.username,
        },
        statusCode: 404,
        trace: trace,
      };

      logger.info(logDataNoCategoryId);

      res.status(404).json(errorRes);
      return;
    }

    const isValidId = await categoryController.isValidCategoryId(categoryId);

    if (!isValidId) {
      const errorCode = 'bad_request';
      const errorMessage = 'Invalid Category ID';
      const moreInfo = 'Please provide a valid category ID for deletion';

      const errorRes = errorResponse(errorCode, errorMessage, moreInfo, trace);

      const logDataInvalidCategoryId = {
        level: 'info',
        message: 'Invalid category ID provided for deletion',
        operation: 'DELETE',
        '@timestamp': new Date().toISOString(),
        'ecs.version': '1.11.0',
        event: {
          category: 'user_interaction',
          type: 'delete',
        },
        user: {
          id: req.userId,
          username: req.username,
        },
        statusCode: 404,
        trace: trace,
      };

      logger.info(logDataInvalidCategoryId);

      res.status(404).json(errorRes);
      return;
    }

    const activeExpenses = await categoryController.getActiveExpensesByCategoryId(categoryId);

    if (activeExpenses.length > 0) {
      const errorCode = 'bad_request';
      const errorMessage = 'Deletion not possible due to active expenses';
      const moreInfo = 'Please delete or update active expenses related to this category before proceeding with deletion';

      const errorRes = errorResponse(errorCode, errorMessage, moreInfo, trace);

      const logDataActiveExpenses = {
        level: 'info',
        message: 'Deletion not possible due to active expenses',
        operation: 'DELETE',
        '@timestamp': new Date().toISOString(),
        'ecs.version': '1.11.0',
        event: {
          category: 'user_interaction',
          type: 'delete',
        },
        user: {
          id: req.userId,
          username: req.username,
        },
        statusCode: 400,
        trace: trace,
      };

      logger.info(logDataActiveExpenses);

      res.status(400).json(errorRes);
      return;
    }

    await categoryController.deleteCategory(categoryId);

    const timestamp = new Date().toISOString();

    const logDataDeletedCategory = {
      level: 'info',
      message: `Delete operation for Category ID: ${categoryId}`,
      operation: 'DELETE',
      '@timestamp': timestamp,
      'ecs.version': '1.11.0',
      event: {
        category: 'user_interaction',
        type: 'delete',
      },
      user: {
        id: req.userId,
        username: req.username,
      },
      object: {
        type: 'Category',
        id: categoryId,
      },
      statusCode: 204,
      trace: trace,
    };

    logger.info(logDataDeletedCategory);

    res.status(204).json({ message: "Successful operation." });
  } catch (error) {
    logger.error('Error occurred:', error);
    res.status(500).json({ error: 'Internal Server Error' });
  }
});

// ----- EXPENSES -----

// GET /expenses
router.get('/expenses', authenticateJWT_optional, async (req, res) => {
  const trace = generateUUID();
  try {
    const includeDeleted = req.query.include_deleted || false; // Default value: false
    const categoryId = req.query.category_id;

    const isValidCategory = await categoryController.isValidCategoryId(categoryId);

    if (!isValidCategory) {
      const errorCode = 'bad_request';
      const errorMessage = 'Invalid category ID';
      const moreInfo = 'Please provide a valid category ID for fetching expenses';

      const errorRes = errorResponse(errorCode, errorMessage, moreInfo, trace);

      const logDataInvalidCategoryId = {
        level: 'info',
        message: 'Invalid category ID provided for fetching expenses',
        operation: 'GET',
        '@timestamp': new Date().toISOString(),
        'ecs.version': '1.11.0',
        event: {
          category: 'user_interaction',
          type: 'read',
        },
        user: {
          id: req.userId || 'Anonymous',
          username: req.username || 'Unknown',
        },
        statusCode: 400,
        trace: trace,
      };

      logger.info(logDataInvalidCategoryId);

      return res.status(400).json(errorRes);
    }

    let expenses;
    if (includeDeleted === "true") {
      expenses = await categoryController.getAllExpensesByCategoryId()
    } else {
      expenses = await categoryController.getActiveExpensesByCategoryId();
    }

    if (!expenses || expenses.length === 0) {
      const errorCode = 'not_found';
      const errorMessage = 'No expenses found for this category';
      const moreInfo = 'There are no expenses available for the provided category ID';

      const errorRes = errorResponse(errorCode, errorMessage, moreInfo, trace);

      const logDataNoExpenses = {
        level: 'info',
        message: 'No expenses found for this category',
        operation: 'GET',
        '@timestamp': new Date().toISOString(),
        'ecs.version': '1.11.0',
        event: {
          category: 'user_interaction',
          type: 'read',
        },
        user: {
          id: req.userId || 'Anonymous',
          username: req.username || 'Unknown',
        },
        object: {
          type: 'Expense',
          id: '', // No expense ID available
        },
        statusCode: 404,
        trace: trace,
      };

      logger.info(logDataNoExpenses);

      return res.status(404).json(errorRes);
    }

    const expenseIds = expenses.map(expense => expense.id).join(',');
    const timestamp = new Date().toISOString();

    const logData = {
      level: 'info',
      message: 'Read operation',
      operation: 'GET',
      '@timestamp': timestamp,
      'ecs.version': '1.11.0',
      event: {
        category: 'user_interaction',
        type: 'read',
      },
      user: {
        id: req.userId || 'Anonymous',
        username: req.username || 'Unknown',
      },
      object: {
        type: 'Expense',
        id: expenseIds,
      },
      statusCode: 200,
      trace: trace,
    };

    logger.log(logData);

    res.status(200).json({ expenses });
  } catch (error) {
    const logDataError = {
      level: 'error',
      message: 'Error occurred',
      operation: 'GET',
      '@timestamp': new Date().toISOString(),
      'ecs.version': '1.11.0',
      event: {
        category: 'user_interaction',
        type: 'read',
      },
      user: {
        id: req.userId || 'Anonymous',
        username: req.username || 'Unknown',
      },
      statusCode: 500,
      error,
      trace: trace,
    };

    logger.error(logDataError);
    res.status(500).json({ error: 'Internal server error' });
  }
});

// POST /expenses
router.post('/expenses', authenticateJWT, async (req, res) => {
  const trace = generateUUID();
  try {
    const { category_id, expense_value, expense_date, expense_note } = req.body;
    const username = "biletado";

    // Validate input
    if (!category_id || !expense_value || !expense_date) {
      const errorCode = 'bad_request';
      const errorMessage = 'Category ID, expense value, and expense date are required';
      const moreInfo = 'Please provide all required fields: category_id, expense_value, and expense_date';

      const errorRes = errorResponse(errorCode, errorMessage, moreInfo, trace);

      const logDataInvalidInput = {
        level: 'info',
        message: 'Invalid input for creating expense',
        operation: 'POST',
        '@timestamp': new Date().toISOString(),
        'ecs.version': '1.11.0',
        event: {
          category: 'user_interaction',
          type: 'create',
        },
        user: {
          id: req.userId,
          username: req.username,
        },
        statusCode: 400,
        trace: trace,
      };

      logger.info(logDataInvalidInput);

      return res.status(400).json(errorRes);
    }

    // Check if the category ID is valid
    const isValidCategory = await categoryController.isValidCategoryId(category_id);

    if (!isValidCategory) {
      const errorCode = 'bad_request';
      const errorMessage = 'Invalid category ID or the category does not exist or is deleted';
      const moreInfo = 'Please provide a valid category ID that exists and is active';

      const errorRes = errorResponse(errorCode, errorMessage, moreInfo, trace);

      const logDataInvalidCategory = {
        level: 'info',
        message: 'Invalid category ID provided for creating expense',
        operation: 'POST',
        '@timestamp': new Date().toISOString(),
        'ecs.version': '1.11.0',
        event: {
          category: 'user_interaction',
          type: 'create',
        },
        user: {
          id: req.userId,
          username: req.username,
        },
        statusCode: 400,
        trace: trace,
      };

      logger.info(logDataInvalidCategory);

      return res.status(400).json(errorRes);
    }

    const newExpense = await expenseController.createExpense(category_id, expense_value, expense_date, expense_note, username);
    const timestamp = new Date().toISOString();

    const logDataCreatedExpense = {
      level: 'info',
      message: 'Create expense operation',
      operation: 'POST',
      '@timestamp': timestamp,
      'ecs.version': '1.11.0',
      event: {
        category: 'user_interaction',
        type: 'create',
      },
      user: {
        id: req.userId,
        username: req.username,
      },
      object: {
        type: 'Expense',
        id: newExpense.id,
      },
      statusCode: 201,
      trace: trace,
    };

    logger.info(logDataCreatedExpense);

    res.status(201).json(newExpense);
  } catch (error) {
    const logDataError = {
      level: 'error',
      message: 'Error occurred',
      operation: 'POST',
      '@timestamp': new Date().toISOString(),
      'ecs.version': '1.11.0',
      event: {
        category: 'user_interaction',
        type: 'create',
      },
      user: {
        id: req.userId,
        username: req.username,
      },
      statusCode: 500,
      error,
      trace: trace,
    };

    logger.error(logDataError);
    res.status(500).json({ error: 'Internal server error' });
  }
});

// GET /expenses/{id}
router.get('/expenses/:id', authenticateJWT_optional, async (req, res) => {
  const trace = generateUUID();
  try {
    const expenseId = req.params.id;

    const isValidExpense = await expenseController.isValidExpenseId(expenseId);
    if (!isValidExpense) {
      const errorCode = 'not_found';
      const errorMessage = 'Expense not found';
      const moreInfo = 'No expense found with the provided ID';

      const errorRes = errorResponse(errorCode, errorMessage, moreInfo, trace);

      const logDataInvalidExpenseId = {
        level: 'info',
        message: 'Invalid expense ID provided for fetching expense',
        operation: 'GET',
        '@timestamp': new Date().toISOString(),
        'ecs.version': '1.11.0',
        event: {
          category: 'user_interaction',
          type: 'read',
        },
        user: {
          id: req.userId || 'Anonymous',
          username: req.username || 'Unknown',
        },
        object: {
          type: 'Expense',
          id: expenseId,
        },
        statusCode: 404,
        trace: trace,
      };

      logger.info(logDataInvalidExpenseId);

      return res.status(404).json(errorRes);
    }

    const expense = await expenseController.getExpenseById(expenseId);
    const timestamp = new Date().toISOString();

    const logData = {
      level: 'info',
      message: 'Get expense operation',
      operation: 'GET',
      '@timestamp': timestamp,
      'ecs.version': '1.11.0',
      event: {
        category: 'user_interaction',
        type: 'read',
      },
      user: {
        id: req.userId || 'Anonymous',
        username: req.username || 'Unknown',
      },
      object: {
        type: 'Expense',
        id: expenseId,
      },
      statusCode: 200,
      trace: trace,
    };

    logger.log(logData);

    res.status(200).json(expense);
  } catch (error) {
    const logDataError = {
      level: 'error',
      message: 'Error occurred',
      operation: 'GET',
      '@timestamp': new Date().toISOString(),
      'ecs.version': '1.11.0',
      event: {
        category: 'user_interaction',
        type: 'read',
      },
      user: {
        id: req.userId || 'Anonymous',
        username: req.username || 'Unknown',
      },
      statusCode: 500,
      error,
      trace: trace,
    };

    logger.error(logDataError);
    res.status(500).json({ error: 'Internal server error' });
  }
});


// PUT /expenses/{id}
router.put('/expenses/:id', authenticateJWT, async (req, res) => {
  const trace = generateUUID();
  try {
    const expenseId = req.params.id;
    const { category_id, expense_value, expense_date, expense_note } = req.body;
    const username = "biletado";

    // Check if the expense ID is valid
    const isValidExpense = await expenseController.isValidExpenseId(expenseId);

    if (!isValidExpense) {
      const newExpense = await expenseController.createExpense(category_id, expense_value, expense_date, expense_note, username);

      const logDataCreateExpense = {
        level: 'info',
        message: 'Create expense operation',
        operation: 'PUT',
        '@timestamp': new Date().toISOString(),
        'ecs.version': '1.11.0',
        event: {
          category: 'user_interaction',
          type: 'create',
        },
        user: {
          id: req.userId,
          username: req.username,
        },
        object: {
          type: 'Expense',
          id: expenseId,
        },
        statusCode: 201,
        trace: trace,
      };

      logger.info(logDataCreateExpense);

      return res.status(201).json(newExpense);
    }

    const expenseInfo = await expenseController.getExpenseById(expenseId);

    if (expenseInfo.deleted_at && expenseInfo.deleted_at !== null) {
      const errorCode = 'bad_request';
      const errorMessage = 'Expense was deleted and cannot be updated without restoration';
      const moreInfo = 'Please restore the expense before updating it.';

      const errorRes = errorResponse(errorCode, errorMessage, moreInfo, trace);

      const logDataDeletedExpense = {
        level: 'info',
        message: 'Deleted expense cannot be updated',
        operation: 'PUT',
        '@timestamp': new Date().toISOString(),
        'ecs.version': '1.11.0',
        event: {
          category: 'user_interaction',
          type: 'update',
        },
        user: {
          id: req.userId,
          username: req.username,
        },
        object: {
          type: 'Expense',
          id: expenseId,
        },
        statusCode: 400,
        trace: trace,
      };

      logger.info(logDataDeletedExpense);

      return res.status(400).json(errorRes);
    }

    const updatedExpense = await expenseController.updateExpense(expenseId, category_id, expense_value, expense_date, expense_note, username);

    const logDataUpdateExpense = {
      level: 'info',
      message: 'Update expense operation',
      operation: 'PUT',
      '@timestamp': new Date().toISOString(),
      'ecs.version': '1.11.0',
      event: {
        category: 'user_interaction',
        type: 'update',
      },
      user: {
        id: req.userId,
        username: req.username,
      },
      object: {
        type: 'Expense',
        id: expenseId,
      },
      statusCode: 200,
      trace: trace,
    };

    logger.info(logDataUpdateExpense);

    res.status(200).json(updatedExpense);

  } catch (error) {
    const logDataError = {
      level: 'error',
      message: 'Error occurred',
      operation: 'PUT',
      '@timestamp': new Date().toISOString(),
      'ecs.version': '1.11.0',
      event: {
        category: 'user_interaction',
        type: 'create',
      },
      user: {
        id: req.userId,
        username: req.username,
      },
      statusCode: 500,
      error,
      trace: trace,
    };

    logger.error(logDataError);
    res.status(500).json({ error: 'Internal server error' });
  }
});

// DELETE /expenses/{id}
router.delete('/expenses/:id', authenticateJWT, async (req, res) => {
  const trace = generateUUID();
  try {
    const expenseId = req.params.id;

    if (!expenseId) {
      const errorCode = 'bad_request';
      const errorMessage = 'Expense ID not provided';
      const moreInfo = 'Please provide a valid expense ID for deletion';

      const errorRes = errorResponse(errorCode, errorMessage, moreInfo, trace);

      const logDataNoExpenseId = {
        level: 'info',
        message: 'No expense ID provided for deletion',
        operation: 'DELETE',
        '@timestamp': new Date().toISOString(),
        'ecs.version': '1.11.0',
        event: {
          category: 'user_interaction',
          type: 'delete',
        },
        user: {
          id: req.userId,
          username: req.username,
        },
        statusCode: 400,
        trace: trace,
      };

      logger.info(logDataNoExpenseId);

      res.status(400).json(errorRes);
      return;
    }

    const isValidExpense = await expenseController.isValidExpenseId(expenseId);

    if (!isValidExpense) {
      const errorCode = 'not_found';
      const errorMessage = 'Expense not found or already deleted';
      const moreInfo = 'Expense may not exist or has been soft-deleted previously.';

      const errorRes = errorResponse(errorCode, errorMessage, moreInfo, trace);

      const logDataInvalidExpense = {
        level: 'info',
        message: 'Invalid expense ID for deletion',
        operation: 'DELETE',
        '@timestamp': new Date().toISOString(),
        'ecs.version': '1.11.0',
        event: {
          category: 'user_interaction',
          type: 'delete',
        },
        user: {
          id: req.userId,
          username: req.username,
        },
        object: {
          type: 'Expense',
          id: expenseId,
        },
        statusCode: 404,
        trace: trace,
      };

      logger.info(logDataInvalidExpense);

      return res.status(404).json(errorRes);
    }

    await expenseController.deleteExpense(expenseId);

    const logDataSoftDeleteExpense = {
      level: 'info',
      message: 'Soft-delete expense operation',
      operation: 'DELETE',
      '@timestamp': new Date().toISOString(),
      'ecs.version': '1.11.0',
      event: {
        category: 'user_interaction',
        type: 'delete',
      },
      user: {
        id: req.userId,
        username: req.username,
      },
      object: {
        type: 'Expense',
        id: expenseId,
      },
      statusCode: 204,
      trace: trace,
    };

    logger.info(logDataSoftDeleteExpense);

    res.status(204).json({ message: "successful operation." });
  } catch (error) {
    const logDataError = {
      level: 'error',
      message: 'Error occurred',
      operation: 'DELETE',
      '@timestamp': new Date().toISOString(),
      'ecs.version': '1.11.0',
      event: {
        category: 'user_interaction',
        type: 'delete',
      },
      user: {
        id: req.userId,
        username: req.username,
      },
      statusCode: 500,
      error,
      trace: trace,
    };

    logger.error(logDataError);

    res.status(500).json({ error: 'Internal server error' });
  }
});

module.exports = router;