// index.js
const express = require('express');
const bodyParser = require('body-parser');
const routes = require('./routes/routes');

const app = express();
app.use(bodyParser.json());

const PORT = process.env.EXPRESS_SERVER_PORT || 9000;

const EXPRESS_APPLICATION_PATH = process.env.EXPRESS_APPLICATION_PATH || '/api/v1/expenses'; // Define the base path

app.use(EXPRESS_APPLICATION_PATH, routes);


app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}`);
});