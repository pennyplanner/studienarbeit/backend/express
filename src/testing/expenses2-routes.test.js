const request = require('supertest');
const express = require('express');
const router = require('../routes/routes');

const app = express();
app.use(express.json());
app.use('/', router);

describe('Expense API Endpoints', () => {
   // Sending a request to retrieve expenses and expecting a 200 status code
  it('should return expenses with status 200', async () => {
    const response = await request(app).get('/api/v1/expenses/expenses');
    expect(response.statusCode).toBe(200);
    expect(response.body).toHaveProperty('expenses');
    expect(Array.isArray(response.body.expenses)).toBe(true);
  });

    // Creating a new expense and expecting a 201 status code
  it('should create an expense with status 201', async () => {
    const newExpense = {
      category_id: 'categoryId',
      expense_value: 50,
      expense_date: '2024-01-05',
      expense_note: 'Test expense',
    };
    const response = await request(app)
      .post('/api/v1/expenses/expenses')
      .send(newExpense);
    expect(response.statusCode).toBe(201);
    expect(response.body).toHaveProperty('id');
    expect(response.body.expense_value).toBe(newExpense.expense_value);
  });

  // Sending a request to retrieve a specific expense and expecting a 200 status code
  it('should return a specific expense with status 200', async () => {
    const expenseId = 'expenseId';
    const response = await request(app).get(`/api/v1/expenses/expenses/${expenseId}`);
    expect(response.statusCode).toBe(200);
    expect(response.body).toHaveProperty('id', expenseId);
  });

    // Updating an existing expense and expecting a 200 status code
  it('should update an expense with status 200', async () => {
    const expenseId = 'expenseId';
    const updatedExpense = {
      category_id: 'categoryId',
      expense_value: 75,
      expense_date: '2024-01-06',
      expense_note: 'Updated test expense',
    };
    const response = await request(app)
      .put(`/api/v1/expenses/expenses/${expenseId}`)
      .send(updatedExpense);
    expect(response.statusCode).toBe(200);
    expect(response.body).toHaveProperty('id', expenseId);
    expect(response.body.expense_value).toBe(updatedExpense.expense_value);
  });

  
  // Soft-deleting an expense and expecting a 204 status code
  it('should soft-delete an expense with status 204', async () => {
    const expenseId = 'expenseId';
    const response = await request(app).delete(`/api/v1/expenses/expenses/${expenseId}`);
    expect(response.statusCode).toBe(204);
  });
});
