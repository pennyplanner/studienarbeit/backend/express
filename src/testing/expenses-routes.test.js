const request = require('supertest');
const express = require('express');
const router = require('../routes/routes');

const app = express();
app.use(express.json()); // Parse incoming requests with JSON payloads
app.use('/', router);

describe('Expense API Endpoints', () => {
  // Test GET /api/v1/expenses/categories
  it('should return categories with status 200', async () => {
    const response = await request(app).get('/api/v1/expenses/categories');
    
    expect(response.statusCode).toBe(200);
    expect(response.body).toHaveProperty('categories');
    expect(Array.isArray(response.body.categories)).toBe(true);
  });

  // Test POST /api/v1/expenses/categories
  it('should create a new category with status 201', async () => {
    const newCategory = { name: 'Test Category' };
    const response = await request(app)
      .post('/api/v1/expenses/categories')
      .send(newCategory);
    
    expect(response.statusCode).toBe(201);
    expect(response.body).toHaveProperty('id');
    expect(response.body.name).toBe(newCategory.name);
  });

  // Test GET /api/v1/expenses/categories/:id
  it('should return a specific category with status 200', async () => {
    // Assuming '1' is a valid category ID in the test database
    const categoryId = '1';
    const response = await request(app).get(`/api/v1/expenses/categories/${categoryId}`);
    
    expect(response.statusCode).toBe(200);
    expect(response.body).toHaveProperty('id', categoryId);
  });

  // Test PUT /api/v1/expenses/categories/:id
  it('should update a category with status 200', async () => {
    // Assuming '1' is a valid category ID in the test database
    const categoryId = '1';
    const updatedCategory = { name: 'Updated Test Category' };
    const response = await request(app)
      .put(`/api/v1/expenses/categories/${categoryId}`)
      .send(updatedCategory);
    
    expect(response.statusCode).toBe(200);
    expect(response.body).toHaveProperty('id', categoryId);
    expect(response.body.name).toBe(updatedCategory.name);
  });

  // Test DELETE /api/v1/expenses/categories/:id
  it('should delete a category with status 204', async () => {
    // Assuming '1' is a valid category ID in the test database
    const categoryId = '1';
    const response = await request(app).delete(`/api/v1/expenses/categories/${categoryId}`);
    
    expect(response.statusCode).toBe(204);
  });
});
