const request = require('supertest');
const express = require('express');
const router = require('../routes/routes');

// Creating a mock Express app and using the router
const app = express();
app.use('/', router);

describe('Expense API Endpoints', () => {
    it('should return the expenses status', async () => {
        const response = await request(app).get('/api/v1/expenses/status');

        expect(response.statusCode).toBe(200);
        expect(response.body.authors).toEqual(['Marc Griese', 'Miko Heuken']);
        expect(response.body.api_version).toBe('1.0.0');
    });

    //   it('should return the expenses health status', async () => {
    //     const response = await request(app).get('/api/v1/expenses/health');

    //     expect(response.statusCode).toBe(200); 
    //     expect(response.body).toHaveProperty('live', true);
    //     expect(response.body).toHaveProperty('ready', true);
    //     expect(response.body.databases.assets.connected).toBe(true);
    //   });

    // Sending a request to retrieve live status and expecting a 200 status code
    it('should return the expenses live status', async () => {
        const response = await request(app).get('/api/v1/expenses/health/live');

        expect(response.statusCode).toBe(200);
        expect(response.body).toHaveProperty('live', true);
    });

    // Sending a request to retrieve readiness status and expecting a 200 status code
    it('should return the expenses readiness status', async () => {
        const response = await request(app).get('/api/v1/expenses/health/ready');

        expect(response.statusCode).toBe(200);
        expect(response.body).toHaveProperty('ready', true);
    });
});