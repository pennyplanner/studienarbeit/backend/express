const winston = require('winston')
const { ecsFormat } = require('@elastic/ecs-winston-format');

// Customize log levels
const customLevels = {
  levels: {
    error: 0,
    warn: 1,
    info: 2,
    debug: 3,
    verbose: 4,
    silly: 5,
    custom: 6 // Adding a custom log level
  },
  colors: {
    error: 'red',
    warn: 'yellow',
    info: 'green',
    debug: 'blue',
    verbose: 'cyan',
    silly: 'magenta',
    custom: 'white' // Custom level color
  }
};

const logger = winston.createLogger({
  levels: customLevels.levels, // apply custom levels
  level: process.env.LOG_LEVEL || 'info', // use log level from environment variable
  format: winston.format.combine(
    ecsFormat(),
    winston.format.timestamp()
  ),
  transports: [
    new winston.transports.File({ filename: 'error.log', level: 'error' }),
    new winston.transports.File({ filename: 'combined.log' }),
    new winston.transports.Console()
  ],
});

module.exports = logger;