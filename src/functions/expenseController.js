// expenseController.js
const { query } = require('../database/db');
const logger = require('../logging/logger');

// function to create a new category by categoryId, value, date, note and username
async function createExpense(categoryId, value, date, note, username) {
    const insertQuery = 'INSERT INTO expenses (category_id, value, date, note, username) VALUES ($1, $2, $3, $4, $5) RETURNING id, category_id, value, date, note, username';
    const values = [categoryId, value, date, note, username];

    try {
        const result = await query(insertQuery, values);
        logger.info({
            message: `Expense created - Category ID: ${categoryId}, Value: ${value}, Date: ${date}, Note: ${note}, Username: ${username}`,
            query: insertQuery,
            result: result.rows[0]
        });
        return result.rows[0];
    } catch (error) {
        logger.error(`Error creating expense: ${error.message}`);
        throw new Error(`Error creating expense: ${error.message}`);
    }
}

// function to get a specific expense by its ID
async function getExpenseById(expenseId) {
    const selectQuery = 'SELECT id, category_id, value, date, note, username FROM expenses WHERE id = $1';
    const values = [expenseId];

    try {
        const result = await query(selectQuery, values);
        logger.info({
            message: `Fetched expense by ID: ${expenseId}`,
            query: selectQuery,
            result: result.rows[0]
        });
        return result.rows[0];
    } catch (error) {
        logger.error(`Error fetching expense by ID: ${error.message}`);
        throw new Error(`Error fetching expense by ID: ${error.message}`);
    }
}

// function to update a expense's values by its ID, category ID, value, date, note and username
async function updateExpense(expenseId, categoryId, value, date, note, username) {
    const updateQuery = 'UPDATE expenses SET category_id = $1, value = $2, date = $3, note = $4, username = $5 WHERE id = $6 RETURNING id, category_id, value, date, note, username';
    const values = [categoryId, value, date, note, username, expenseId];

    try {
        const result = await query(updateQuery, values);
        logger.info({
            message: `Updated expense - Expense ID: ${expenseId}, Category ID: ${categoryId}, Value: ${value}, Date: ${date}, Note: ${note}, Username: ${username}`,
            query: updateQuery,
            result: result.rows[0]
        });
        return result.rows[0];
    } catch (error) {
        logger.error(`Error updating expense: ${error.message}`);
        throw new Error(`Error updating expense: ${error.message}`);
    }
}

// function to delete a category by its ID
async function deleteExpense(expenseId) {
    const softDeleteQuery = 'UPDATE expenses SET deleted_at = CURRENT_TIMESTAMP WHERE id = $1';
    const values = [expenseId];

    try {
        await query(softDeleteQuery, values);
        logger.info({
            message: `Deleted expense with ID: ${expenseId}`,
            query: softDeleteQuery
        });
    } catch (error) {
        logger.error(`Error deleting expense: ${error.message}`);
        throw new Error(`Error deleting expense: ${error.message}`);
    }
}

// function to check if user-provided category ID is in valid format and exists in database
async function isValidExpenseId(expenseId) {
    const UUID_REGEX = /^[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-4[0-9a-fA-F]{3}-[89abAB][0-9a-fA-F]{3}-[0-9a-fA-F]{12}$/;
    if (!UUID_REGEX.test(expenseId)) {
        logger.error(`UUID Test failed for expense (ID: ${expenseId})`);
        return false;
    }

    const selectQuery = 'SELECT COUNT(*) FROM expenses WHERE id = $1 AND deleted_at IS NULL';
    const values = [expenseId];

    try {
        const result = await query(selectQuery, values);
        const count = parseInt(result.rows[0].count, 10);
        if (count === 1) {
            logger.info({
                message: `Expense ID is valid: ${expenseId}`,
                query: selectQuery,
                result: result.rows
            });
        } else {
            logger.info({
                message: `Expense ID is NOT valid: ${expenseId}`,
                query: selectQuery,
                result: result.rows
            });
        }
        return count === 1;
    } catch (error) {
        logger.error(`Error while checking expense ID: ${error.message}`);
        return false;
    }
}

module.exports = {
    createExpense,
    getExpenseById,
    updateExpense,
    deleteExpense,
    isValidExpenseId,
};
