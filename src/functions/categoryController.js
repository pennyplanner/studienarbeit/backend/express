//categoryController.js
const { query } = require('../database/db');
const logger = require('../logging/logger');

// function to get all categories
async function getAllCategories() {
    const selectQuery = 'SELECT id, name FROM categories';

    try {
        const result = await query(selectQuery);
        logger.info({
            message: 'Fetched all categories',
            query: selectQuery,
            result: result.rows,
        });
        return result.rows;
    } catch (error) {
        logger.error(`Error fetching all categories: ${error.message}`);
        throw new Error(`Error fetching all categories: ${error.message}`);
    }
}

// function to get active categories
async function getActiveCategories() {
    const selectQuery = 'SELECT id, name FROM categories WHERE deleted_at IS NULL';

    try {
        const result = await query(selectQuery);
        logger.info({
            message: 'Fetched active categories',
            query: selectQuery,
            result: result.rows,
        });
        return result.rows;
    } catch (error) {
        logger.error(`Error fetching active categories: ${error.message}`);
        throw new Error(`Error fetching active categories: ${error.message}`);
    }
}

// function to create a new category by name
async function createCategory(name) {
    const insertQuery = 'INSERT INTO categories (name) VALUES ($1) RETURNING id, name';
    const values = [name];

    try {
        const result = await query(insertQuery, values);
        logger.info({
            message: `Category created: ${name}`,
            query: insertQuery,
            result: result.rows[0]
        });
        return result.rows[0];
    } catch (error) {
        logger.error(`Error creating category: ${error.message}`);
        throw new Error(`Error creating category: ${error.message}`);
    }
}

// function to get a specific category by its ID
async function getCategoryById(categoryId) {
    const selectQuery = 'SELECT id, name FROM categories WHERE id = $1';
    const values = [categoryId];

    try {
        const result = await query(selectQuery, values);
        logger.info({
            message: `Fetched category by ID: ${categoryId}`,
            query: selectQuery,
            result: result.rows[0]
        });
        return result.rows[0];
    } catch (error) {
        logger.error(`Error fetching category by ID: ${error.message}`);
        throw new Error(`Error fetching category by ID: ${error.message}`);
    }
}

// function to update a category's name by its ID
async function updateCategory(categoryId, newName) {
    const updateQuery = 'UPDATE categories SET name = $1 WHERE id = $2 RETURNING id, name';
    const values = [newName, categoryId];

    try {
        const result = await query(updateQuery, values);
        logger.info({
            message: `Updated category name for ID ${categoryId}`,
            query: updateQuery,
            newName: newName,
            result: result.rows[0]
        });
        return result.rows[0];
    } catch (error) {
        logger.error(`Error updating category: ${error.message}`);
        throw new Error(`Error updating category: ${error.message}`);
    }
}

// function to delete a category by its ID
async function deleteCategory(categoryId) {
    const softDeleteQuery = 'UPDATE categories SET deleted_at = CURRENT_TIMESTAMP WHERE id = $1';
    const values = [categoryId];

    try {
        await query(softDeleteQuery, values);
        logger.info({
            message: `Deleted category with ID: ${categoryId}`,
            query: softDeleteQuery
        });
    } catch (error) {
        logger.error(`Error deleting category: ${error.message}`);
        throw new Error(`Error deleting category: ${error.message}`);
    }
}

// function to get all active expenses of a category by its ID
async function getActiveExpensesByCategoryId(categoryId) {
    const selectQuery = 'SELECT id, value, date, note, username FROM expenses WHERE category_id = $1 AND deleted_at IS NULL';
    const values = [categoryId];

    try {
        const result = await query(selectQuery, values);
        logger.info({
            message: `Fetched active expenses for category with ID: ${categoryId}`,
            query: selectQuery,
            result: result.rows
        });
        return result.rows;
    } catch (error) {
        logger.error(`Error fetching active expenses by category id: ${error.message}`);
        throw new Error(`Error fetching active expenses for category: ${error.message}`);
    }
}

// function to get all (active and soft-deleted) expenses of a category by its ID
async function getAllExpensesByCategoryId(categoryId) {
    const selectQuery = 'SELECT id, value, date, note, username FROM expenses WHERE category_id = $1';
    const values = [categoryId];

    try {
        const result = await query(selectQuery, values);
        logger.info({
            message: `Fetched all expenses for category with ID: ${categoryId}`,
            query: selectQuery,
            result: result.rows
        });
        return result.rows;
    } catch (error) {
        logger.error(`Error fetching all expenses by category id: ${error.message}`);
        throw new Error(`Error fetching all expenses for category: ${error.message}`);
    }
}

// function to check if user-provided category ID is in valid format and exists in database
async function isValidCategoryId(categoryId) {
    // Check if ID format is valid
    const UUID_REGEX = /^[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-4[0-9a-fA-F]{3}-[89abAB][0-9a-fA-F]{3}-[0-9a-fA-F]{12}$/;
    if (!UUID_REGEX.test(categoryId)) {
        logger.error(`UUID Test failed for category (ID: ${categoryId})`);
        return false;
    }

    // Check if ID exists in database and if it's not soft-deleted
    const selectQuery = 'SELECT COUNT(*) FROM categories WHERE id = $1 AND deleted_at IS NULL';
    const values = [categoryId];

    try {
        const result = await query(selectQuery, values);
        const count = parseInt(result.rows[0].count, 10);
        if (count === 1) {
            logger.info({
                message: `Category ID is valid: ${categoryId}`,
                query: selectQuery,
                result: result.rows
            });
        } else {
            logger.info({
                message: `Category ID is NOT valid: ${categoryId}`,
                query: selectQuery,
                result: result.rows
            });
        }
        return count === 1; // If ID was found, return true.
    } catch (error) {
        logger.error(`Error while checking category ID: ${error.message}`);
        return false; // If ID was not found, return false.
    }
}

module.exports = {
    getAllCategories,
    getActiveCategories,
    createCategory,
    getCategoryById,
    updateCategory,
    deleteCategory,
    getActiveExpensesByCategoryId,
    isValidCategoryId,
    getAllExpensesByCategoryId,
};