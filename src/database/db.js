// database.js
const { Pool } = require('pg');

// create database connection via pool
const pool = new Pool({
  user: process.env.POSTGRES_ASSETS_USER,
  host: process.env.POSTGRES_ASSETS_HOST,
  database: process.env.POSTGRES_ASSETS_DBNAME,
  password: process.env.POSTGRES_ASSETS_PASSWORD,
  port: parseInt(process.env.POSTGRES_ASSETS_PORT),
});

// console log to check the connection status
console.log("file db.js:" +
  "\nConnection established with" +
  "\nPOSTGRES_ASSETS_USER: " + process.env.POSTGRES_ASSETS_USER +
  "\nPOSTGRES_ASSETS_HOST: " + process.env.POSTGRES_ASSETS_HOST +
  "\nPOSTGRES_ASSETS_DBNAME: " + process.env.POSTGRES_ASSETS_DBNAME +
  "\nPOSTGRES_ASSETS_PORT: " + process.env.POSTGRES_ASSETS_PORT)

  // event handler for errors
pool.on('error', (err, client) => {
  console.log('Unexpected error on idle PostgreSQL client', err);
  process.exit(-1); 
});

// function export for executing queries
module.exports = {
  query: (text, params) => pool.query(text, params),
};