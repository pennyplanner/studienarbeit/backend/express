# Express API for Biletado

Welcome to this Express.js API! This repository contains a Node.js Express API designed to serve as a backend for biletado. Below, you'll find important information on how to set up and configure this project.

## Project Structure

The project is organized into the following directories:

- **database**: Contains database-related files.
- **functions**: Holds utility functions for the API.
- **logging**: Configurations for logging, including error and combined logs.
- **node_modules**: Node.js modules and dependencies.
- **routes**: Defines the API routes.
- **testing**: Houses testing-related files, including Jest tests.
- **index.js**: The main entry point for the Express API.
- **package-lock.json** and **package.json**: Files containing project metadata and dependencies.

## Getting Started

1. Clone this repository:

   ```bash
   git clone https://gitlab.com/pennyplanner/webeng2/backend/express.git
   ```

2. Navigate to the project directory:

   ```bash
   cd express/src
   ```

3. Install dependencies:

   ```bash
   npm install
   ```

4. Run the API:

   ```bash
   node .
   ```

## Scripts

* test: Run Jest tests for the API using the command:

   ```bash
   npm test
   ```

## Dependencies

This project relies on the following npm packages:

- **@elastic/ecs-winston-format**: v1.5.2
- **cors**: v2.8.5
- **express**: v4.18.2
- **jest**: v29.7.0
- **jsonwebtoken**: v9.0.2
- **jwks-rsa**: v3.1.0
- **pg**: v8.11.3
- **supertest**: v6.3.3
- **winston**: v3.11.0

## Configuration

### Useful properties

This Express.js project's properties can be set using environment variables or a property file. For more information on setting up configurations:

* [Environment Variables in Node.js](https://nodejs.org/en/learn/command-line/how-to-read-environment-variables-from-nodejs)

* [Node.js Configuration Management](https://mehranjnf.medium.com/configuration-management-in-node-js-b0d80d16a029)

| Name                     | Description                        | Default         |
|--------------------------|------------------------------------|-----------------|
| EXPRESS_SERVER_PORT      | The port of the application        | 9000            |
| EXPRESS_APPLICATION_PATH | The base path of the application   | api/v1/expenses |
| POSTGRES_ASSETS_HOST     | Database host name                 | -               |
| POSTGRES_ASSETS_PORT     | Database port                      | -               |
| POSTGRES_ASSETS_DBNAME   | Database name                      | -               |
| POSTGRES_ASSETS_USER     | Database user name                 | -               |
| POSTGRES_ASSETS_PASSWORD | Database password                  | -               |
| LOG_LEVEL                | Log Level for Winston Logger       | info            |
| KEYCLOAK_HOST            | Keycloak host name                 | keycloak        |
| KEYCLOAK_REALM           | Keycloak realm                     | biletado        |

For the log level, set the environment variable `LOG_LEVEL` to one of the following values: `error`, `warn`, `info`, `debug`, `verbose`, `silly`, or `custom` (if customized). <br/>
For a list of log levels in Winston click [here.](https://betterstack.com/community/guides/logging/how-to-install-setup-and-use-winston-and-morgan-to-log-node-js-applications/#log-levels-in-winston) <br/>
For setting custom log levels in Node.js, you might want to check out the [Winston](https://betterstack.com/community/guides/logging/how-to-install-setup-and-use-winston-and-morgan-to-log-node-js-applications/#customizing-log-levels-in-winston) logging libraries. Configuration of these loggers can be done through their respective documentation.

## License

This project is licensed under the MIT License - see the [LICENSE](https://gitlab.com/pennyplanner/webeng2/backend/express/-/blob/main/LICENSE?ref_type=heads) file for details.

## Additional Resources

For more information, check out our [GitLab Repository.](https://gitlab.com/pennyplanner/webeng2/backend/express)
